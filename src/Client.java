import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Client {
    
    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException{
        CurrencyConverter client = (CurrencyConverter)Naming.lookup("//localhost/convert");
        System.out.println(args[2] + " " + args[0] + " is " + client.convert(args[0], args[1], Double.valueOf(args[2])) + " " + args[1]);
    }
    
}