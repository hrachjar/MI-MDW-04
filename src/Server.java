import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

public class Server extends UnicastRemoteObject implements CurrencyConverter {
 
    private static final long serialVersionUID = 1L;
    
    protected Server() throws RemoteException {
        super();
    }
    
    @Override
    public double convert(String from, String to, double amount) throws RemoteException{
    	
    	double result = 0;    	
    	
    	if(from.equals("EUR") && to.equals("USD")) {
    		result = amount * 1.079;
    	} else if(from.equals("EUR") && to.equals("GBP")) {
    		result = amount * 0.86;
    	} else if(from.equals("USD") && to.equals("EUR")) {
    		result = amount * 0.926;
    	} else if(from.equals("USD") && to.equals("GBP")) {
    		result = amount * 0.797;
    	} else if(from.equals("GBP") && to.equals("EUR")) {
    		result = amount * 1.161;
    	} else if(from.equals("GBP") && to.equals("USD")) {
    		result = amount * 1.254;
    	}
        
    	return result;
    	
    }
    
    public static void main(String[] args){
        try {
            LocateRegistry.createRegistry(1094);
            
            Server server = new Server();
            Naming.rebind("//0.0.0.0/convert", server);
            
            System.out.println("Server started...");
        } catch (Exception e){
        	System.out.println("Error: " + e.getLocalizedMessage());
        }
    }
}