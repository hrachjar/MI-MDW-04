
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

public class JDBC extends HttpServlet {
	private static final long serialVersionUID = 1L;

	

    public JDBC() throws SQLException {
        super();
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}
	
	private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();
		Connection con = null;
		try {
			con = DriverManager.getConnection(
					"jdbc:mysql://147.32.233.18:3306/mimdw",
					"mimdw",
					"mimdw");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	    try {
	    	Statement stmt = con.createStatement();
	        ResultSet rs = stmt.executeQuery("SELECT * FROM records");

	        out.print("<table border=1>");
	        out.print("<tr><th>ID</th><th>Type</th><th>Location</th><th>Capacity</th></tr>");
	        while (rs.next()) {
	        	int id = rs.getInt("id");
	        	String type = rs.getString("type");
	        	String location = rs.getString("location");
	        	String capacity = rs.getString("capacity");
	        	
	        	out.print("<tr>");
	            out.print("<td>" + id + "</td><td>" + type + "</td><td>" + location + "</td><td>" + capacity + "</td>");
	            out.print("</tr>");
	        }
	        out.print("</table>");
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }
	}
}